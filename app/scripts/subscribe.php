<?php



# CONFIG
define('_DB_HOST', 'localhost:3306');
define('_DB_NAME', 'subscribed');
define('_DB_USER', 'root');
define('_DB_PASS', 'bitnami');


$subscribe = (isset($_POST['action']) && $_POST['action'] == 'unsubscribe')?false:true;

if ($subscribe){
    $fields = array(
        array('name' => 'form_email', 'valid' => array('require', 'form_email')),

    );
}else{
    $fields = array(
        array('name' => 'unsubscribe_email', 'valid' => array('require', 'email')),
        array('name' => 'confirm', 'valid' => array('require'), 'err_message' => 'Please confirm'),
    );
}

// Connect to database
$connection = mysql_connect(_DB_HOST, _DB_USER, _DB_PASS) or die ('Unable to connect to MySQL server.<br ><br >Please make sure your MySQL login details are correct.');
$db = mysql_select_db(_DB_NAME, $connection) or die ('request "Unable to select database."');

$error_fields = array();
$get = array();
foreach ($fields AS $field){
    $value = isset($_POST[$field['name']])?$_POST[$field['name']]:'';
    if (is_array($value)){
        $value = implode('/ ', $value);
    }
    if (get_magic_quotes_gpc()){
        $value = stripslashes($value);
    }
    $get[$field['name']] = mysql_real_escape_string($value);
    $is_valid = true;
    $err_message = '';
    if (!empty($field['valid'])){
        foreach ($field['valid'] AS $valid) {
            switch ($valid) {
                case 'require':
                    $is_valid = $is_valid && strlen($value) > 0;
                    $err_message = 'Field required';
                    break;
                case 'email':
                    $is_valid = $is_valid && preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $value);
                    $err_message = 'Email required';
                    break;
                default:
                    break;
            }
        }
    }
    if (!$is_valid){
        if (!empty($field['err_message'])){
            $err_message = $field['err_message'];
        }
        $error_fields[] = array('name' => $field['name'], 'message' => $err_message);
    }
}

if (empty($error_fields)){
    if ($subscribe){
        $data = array(
            'email' => "'".$get['email']."'",
            'name' => "'".$get['name']."'",
            'date_subscribe' => 'NOW()',
            'status' => "'T'",
        );
        $sql = "REPLACE INTO subscription_form (`".implode("`, `", array_keys($data))."`) VALUES(".implode(", ", array_values($data)).")";
    }else{
        $sql = "UPDATE subscription_form SET date_unsubscribe = NOW(), status = 'F' WHERE email = '".$get['unsubscribe_email']."'";
    }
    if (!empty($sql)){
        $sql_result = mysql_query ($sql, $connection ) or die ('request "Could not execute SQL query" '.$sql);
    }
    echo (json_encode(array('code' => 'success')));
}else{
    echo json_encode(array('code' => 'failed', 'fields' => $error_fields));
}

?>;