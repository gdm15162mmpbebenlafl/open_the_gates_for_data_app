var myVar;
function startTimer() {
    myVar = setInterval(function(){myTimer()},1000);
    timelimit = maxtimelimit;
}
function myTimer() {
    if (timelimit > 0) {
        curmin=Math.floor(timelimit/60);
        cursec=timelimit%60;
        if (curmin!=0) { curtime=curmin+" minutes and "+cursec+" seconds left"; }
        else { curtime=cursec+" seconds left"; }
        $_('timeleft').innerHTML = curtime;
    } else {
        $_('timeleft').innerHTML = timelimit+' - Out of Time!';
        clearInterval(myVar);
    }
    timelimit--;
}



var pos = 0, posn, choice, correct = 0, rscore = 0;
var maxtimelimit = 20, timelimit = maxtimelimit;  // 20 seconds per question

var questions = [
    [ "Vanaf hoeveel km ga je best met de fiets?", "12km", "7km", "20km", "B" ],
    [ "wat is het traagste vervoersmiddel ?", "Auto", "Trein", "DeLijn", "C" ],
    [ "Mag je met je buzzypass de tram nemen?", "ja", "nee", "geen idee, ik rij zwart", "A" ],
    [ "Wat is de kilometerprijs van een taxi?", "3,10euro/km", "4euro/km", "2,10euro/km", "C" ],
    [ "Hoeveel Taxilocaties zijn er in Gent?", "6", "8", "7", "C" ],
    [ "Sinds wanneer bestaat De Lijn?", "1991", "2000", "1998", "A" ],
    [ "Ik kan een pokemon boete krijgen....", "als ik met de fiets ben", "als ik met de tram ben", "als ik te voet ben", "A" ],
    [ "Mag een auto op gas, in een ondergrondse parking?", "ja", "nee", "enkel op -1", "B" ],
    [ "Mag ik pokemon Go spelen in de auto?", "ja", "nee", "ja, maar niet van Niantic", "C" ],
    [ "Mag je met 2 naast elkaar fietsen?", "ja", "nee", "als 1 ervan een kind is", "A" ]
];
var questionOrder = [];
function setQuestionOrder() {
    questionOrder.length = 0;
    for (var i=0; i<questions.length; i++) { questionOrder.push(i); }
    questionOrder.sort(randOrd);   // alert(questionOrder);  // shuffle display order
    pos = 0;  posn = questionOrder[pos];
}

function $_(IDS) { return document.getElementById(IDS); }
function randOrd() { return (Math.round(Math.random())-0.5); }
function renderResults(){
    var test = $_("test");
    test.innerHTML = "<h2>You got "+correct+" of "+questions.length+" questions correct</h2>";
    $_("test_status").innerHTML = "Test Completed";
    $_('timeleft').innerHTML = '';
    test.innerHTML += '<button onclick="location.reload()">Re-test</a> ';
    setQuestionOrder();
    correct = 0;
    clearInterval(myVar);
    return false;
}
function renderQuestion() {
    var test = $_("test");
    $_("test_status").innerHTML = "Question "+(pos+1)+" of "+questions.length;

    var question = questions[posn][0];
    var chA = questions[posn][1];
    var chB = questions[posn][2];
    var chC = questions[posn][3];
    test.innerHTML = "<h3>"+question+"</h3>";
    test.innerHTML += "<label><input type='radio' name='choices' value='A'> "+chA+"</label><br>";
    test.innerHTML += "<label><input type='radio' name='choices' value='B'> "+chB+"</label><br>";
    test.innerHTML += "<label><input type='radio' name='choices' value='C'> "+chC+"</label><br>";
    test.innerHTML += "<button onclick='checkAnswer()'>Submit Answer</button>";
    timelimit = maxtimelimit;
    clearInterval(myVar);
    startTimer();
}

function checkAnswer(){
    var choices = document.getElementsByName("choices");
    for (var i=0; i<choices.length; i++) {
        if (choices[i].checked) { choice = choices[i].value; }
    }
    rscore++;
    if (choice == questions[posn][4] && timelimit > 0) { correct++; }
    pos++;  posn = questionOrder[pos];
    if (pos < questions.length) { renderQuestion(); } else { renderResults(); }
}

window.onload = function() {
    setQuestionOrder();
    renderQuestion();
}