
function initMap() {

    var ghentCenter = {
        lat: 51.0537027,
        lng: 3.7246371
    };

    var datasets = {
        'lijn1': {
            dataset: 'data/delijnhalteslijn1.json',
            icon: 'images/markers/marker_lijn_1.png'
        },
        'lijn3': {
            dataset: 'data/delijnhalteslijn3.json',
            icon: 'images/markers/marker_lijn_3.png'
        },
        'lijn18': {
            dataset: 'data/delijnhalteslijn18.json',
            icon: 'images/markers/marker_lijn_18.png'
        },
        'lijn21': {
            dataset: 'data/delijnhalteslijn21.json',
            icon: 'images/markers/marker_lijn_21.png'
        },
        'lijn22': {
            dataset: 'data/delijnhalteslijn22.json',
            icon: 'images/markers/marker_lijn_22.png'
        },
        'lijn5': {
            dataset: 'data/delijnhalteslijn5.json',
            icon: 'images/markers/marker_lijn_5.png'
        },
        'lijn4': {
            dataset: 'data/delijnhalteslijn4.json',
            icon: 'images/markers/marker_lijn_4.png'
        }
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: ghentCenter,
        disableDefaultUI: true,
        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]

    });

    $.each(datasets, function(index, object) {

        $.getJSON(object.dataset, function(json) {

            $.each(json.rtLijnRitten, function(i, d) {

                $.each(d.rtDoortochten, function(i, v) {



                    var coords = new google.maps.LatLng(v.coordinaat.lt, v.coordinaat.ln);



                    //console.log(d);
                    console.log(v);

                    var html = '<p><strong>'+ 'LIJN: ' + d.omschrijving +'</strong><br />';
                    html += 'haltenummer: ' + v.halteNummer + '<br />';
                    html += v.omschrijvingLang + '</p>';


                    var infowindow = new google.maps.InfoWindow({
                        content:   html

                    });

                    var marker = new google.maps.Marker({
                        position: coords,
                        map: map,
                        title: v.omschrijvingLang,
                        icon: object.icon
                    });

                    marker.addListener('click', function(e) {
                        infowindow.open(map, marker);
                    });

                });

            });

        });

    });

}
